<?php
function fail($reason) {
    echo $reason;
    echo "\n";
    exit(1);
}
if ($argc > 1) {
    if (!is_file($argv[1])) fail($argv[1]. ' <- is not a file');
    $in_filename = $argv[1];
    $out_filename = substr_replace($in_filename, 'opml', -4);
} else fail('Usage: php '.$argv[0].' file.json');
$json = json_decode(file_get_contents($in_filename), true);
if (!is_array($json)) fail('failed to parse json at: '.$in_filename);
//put into array to deduplicate
$channels = [];
foreach ($json as $channel) {
    $channels[$channel['snippet']['resourceId']['channelId']] = str_replace('"', '', $channel['snippet']['title']);
}
ob_start();
?><?xml version="1.0" encoding="UTF-8"?>
<opml version="1.0">
<head>
<title>Yt Subscriptions</title>
</head>
<body>
<?php foreach ($channels as $id => $title) : ?>
<outline text="<?=$title?>" title="<?=$title?>" type="rss" xmlUrl="https://www.youtube.com/feeds/videos.xml?channel_id=<?=$id?>" htmlUrl="https://www.youtube.com/channel/<?=$id?>"/>
<?php endforeach; ?>
</body>
</opml>
<?php
$text = ob_get_contents();
ob_clean();

$write_result = file_put_contents($out_filename, $text);
if ($write_result === false) fail('failed to write file '.$out_filename);
echo 'Success, result in '.$out_filename."\n";
